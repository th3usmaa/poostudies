package br.ucsal.bes20181.poo.avaliacao1;

public class Veiculos {

	private String placa;
	private Integer anoFabricacao;
	private TipoVeiculoENUM tipo;

	public TipoVeiculoENUM getTipo() {
		return tipo;
	}

	public void setTipo(TipoVeiculoENUM tipo) {
		this.tipo = tipo;
	}

	public String getPlaca() {
		return placa;
	}

	public Integer getAnoFabricacao() {
		return anoFabricacao;
	}

	public Veiculos(String placa, Integer anoFabricacao, TipoVeiculoENUM tipo) {

		this.placa = placa;
		this.anoFabricacao = anoFabricacao;
		this.tipo = tipo;
	}

}
