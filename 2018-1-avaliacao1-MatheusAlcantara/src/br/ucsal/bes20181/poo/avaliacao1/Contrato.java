package br.ucsal.bes20181.poo.avaliacao1;

import java.util.List;

public class Contrato {

	private static Integer cont = 0;
	private double aa = 0;
	private Integer numContrato;
	private String nomeCliente;
	private String endCliente;
	private List<Veiculo> veiculos;
	private double valorContrato;

	public Contrato(String nomeCliente, String endCliente) {

		this.nomeCliente = nomeCliente;
		this.endCliente = endCliente;
		gerarNumeroContrato();
	}

	private void gerarNumeroContrato() {
		cont++;
		this.numContrato = cont;

	}

	public String getNomeCliente() {
		return nomeCliente;
	}

	public String getEndCliente() {
		return endCliente;
	}

	public double getValorContrato() {
		return valorContrato;
	}

	public Boolean adicionarVeiculo(Veiculo veiculo) {

	}

}
